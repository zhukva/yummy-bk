<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [];
        for ($i=0; $i < 20; $i++) {
            $components_arr = [];
            for ($j=0; $j < 6; $j++) {
                $components[] = str_random(7);
            }
            DB::table('recipes')->insert([
                'title' => str_random(10),
                'about' => str_random(30),
                'components' => implode(',', $components)
            ]);
        }
    }
}
