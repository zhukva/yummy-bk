<?php

namespace App\Http\Controllers;

use App\Recipe;
use Illuminate\Http\Request;

class RecipeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function index()
    {
        $items = Recipe::all();
        return response()->json($items);
    }

    public function create(Request $request)
    {
        $recipe = Recipe::create($request->json()->all());
        return response()->json($recipe, 201);
    }

    public function delete($id)
    {
        Recipe::findOrFail($id)->delete();
        return response()->json($id, 200);
    }

    public function update($id, Request $request)
    {
        $recipe = Recipe::findOrFail($id);
        $recipe->update($request->json()->all());
        return response()->json($recipe, 200);
    }
}
